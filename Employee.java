public class Employee implements IEmployee {

  private int employeeID;
  private String employeeName;
  private int managerID;

  public Employee(String employeeID, String employeeName, String managerID) {
    if (managerID.equals("")) { 
      managerID = "0";
    }
    this.employeeID = Integer.valueOf(employeeID);
    this.employeeName = employeeName;
    this.managerID = Integer.valueOf(managerID);
  }

  public int getEmployeeID() {
    return employeeID;
  }

  public String getEmployeeName() {
    return employeeName;
  }

  public int getManagerID() {
    return managerID;
  }
}