
1. Unzip the file BTCodingTest. The zipped file BTCodingTest.zip has the following files
superheroes.txt, README.txt, Employee.java, IEmployee.java and PathFinder.java

2. Open the command line/command-prompt and use the cd command to navigate to the source file location/folder. 

3. From the command-line/command-prompt type the following command to compile all three java files.
javac *.java

4. Next step should be to run the PathFinder program type the following  command.
java PathFinder superheroes.txt "Batman" "Catwoman"

5. Expected Results will be as follows:
Batman(16) -> Black Widow(6) <- Catwoman(17)