import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PathFinder {

  private static final Set<IEmployee> employees = new HashSet<IEmployee>();

  /** Regex pattern inside the split method "|" to read only the strings is
  *inspired from one of the questions on the site 'stackoverflow.com'.
  */

  public static void main(String[] args) {
    BufferedReader br;
    try {
      br = new BufferedReader(new FileReader(args[0]));
      String line;
      br.readLine();
      while ((line = br.readLine()) != null) {
        String[] inputs = line.split("\\|");
        employees.add(new Employee(inputs[1].trim(), inputs[2].trim(), inputs[3].trim()));
      }
      br.close();
    } catch (IOException e) {
      e.printStackTrace();
    }

    findShortestCommunicationPathBetween(args[1], args[2]);
  }

  /**
   * @param srcEmployeeName
   * @param destEmployeeName
   */
  private static void findShortestCommunicationPathBetween(String srcEmployeeName, String destEmployeeName) {
    List<IEmployee> srcCommunicationPath = getCommunicationPathToTopFor(srcEmployeeName);
    List<IEmployee> destCommunicationPath = getCommunicationPathToTopFor(destEmployeeName);

    int commonManagerID = -1;
    for (IEmployee employee : srcCommunicationPath) {
      if (destCommunicationPath.contains(employee)) {
        commonManagerID = employee.getEmployeeID();
        break;
      }
    }

    System.out.println();
    for (IEmployee employee : srcCommunicationPath) {
      System.out.print(employee.getEmployeeName() + "(" + employee.getEmployeeID() + ")");
      if (commonManagerID == employee.getEmployeeID()) {
        break;
      } else {
        System.out.print(" -> ");
      }
    }

    List<IEmployee> toReverse = new ArrayList<IEmployee>();
    for (IEmployee employee : destCommunicationPath) {
      if (commonManagerID != employee.getEmployeeID()) {
        toReverse.add(employee);
      } else {
        break;
      }
    }

    /** To reverse the list, Collections.reverse method , 
    * has been inspired from 'stackoverflow.com' and package imported from import java.util.Collections 
    */

    Collections.reverse(toReverse);
    for (IEmployee employee : toReverse) {
      if (commonManagerID != employee.getEmployeeID()) {
        System.out.print(" <- ");
        System.out.print(employee.getEmployeeName() + "(" + employee.getEmployeeID() + ")");
      } else {
        break;
      }
    }
	System.out.println();
  }

  private static List<IEmployee> getCommunicationPathToTopFor(String employeeName) {
	List<IEmployee> communicationPath = new ArrayList<IEmployee>();
	IEmployee iEmployee = null;
	for (IEmployee employee : employees) {
	  if (employee.getEmployeeName().equals(employeeName)) {
		iEmployee = employee;
	  }
	}
	if(iEmployee != null) {
		while (iEmployee.getManagerID() >= 0 && !communicationPath.contains(iEmployee)) {
		  communicationPath.add(iEmployee);
		  int managerID = iEmployee.getManagerID();
		  for (IEmployee employee : employees) {
			if (employee.getEmployeeID() == managerID) {
			  iEmployee = employee;
			  break;
			}
		  }
		}
	}
    return communicationPath;
  }
}