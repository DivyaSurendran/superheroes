public interface IEmployee {
	
  public int getEmployeeID();
  public String getEmployeeName();
  public int getManagerID();
}
